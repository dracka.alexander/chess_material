Chess material
==========================================================================
My own content of presentations generated in LaTeX, analyses of chess games, opening and positions.

List of my own live games. Chess scid databases for learning purpose and deep analyses with Stockfish 15 locally.

User-friendly analyses could be found in my [lichess collection](https://lichess.org/study/fqNX4883) of my live games

Presentations
-------------
- interesting variants of my most played openings


Analysis
--------
- analyses of my own games (live or online daily game) with the engine


My games
--------
- list of my own live games in pgn format.

Scid databases
--------------
- various databases for learning and analyses of position and theory (eg. Endgame based on Dvoretsky's Endgame Manual)
- list of all my games from chess.com platform ( bullet, blitz, rapid and daily). Which could be used for analyses of 
my common mistakes, blunders and missed moves.
Please note: these databases were created and can be open with [Scid vs. PC](https://scidvspc.sourceforge.net/) 


*Created with love for a game which took me by a heart.*